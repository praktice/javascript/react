import Auth from './Auth';
import Dashboard from './Dashboard';
import Home from './Home';
import About from './About';
import NotFound from './NotFound';
import Privacy from './Privacy';
import Terms from './Terms';

export default {
  Auth,
  Dashboard,
  Home,
  About,
  NotFound,
  Privacy,
  Terms,
}

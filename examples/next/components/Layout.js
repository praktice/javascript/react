import Head from 'next/head'
import Hero from './partials/Hero'
import Nav from './partials/Nav'
import Footer from './partials/Footer'

export default function Layout({ children, pageTitle, ...props }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>{pageTitle}</title>
      </Head>
        <Nav />
        <Hero pageTitle={pageTitle} />
        <section className="content">
          <div className="column is-8 is-offset-2">{children}</div>
        </section>
      <Footer />
    </>
  )
}

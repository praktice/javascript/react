import React from 'react';
import {PublicLayout as Layout} from '../layouts';
import Header from '../components/Header';

const Home = () => {
  return (
    <Layout>
    <Header title="Home" />
    <p>
      Hello
    </p>
    </Layout>
  );
};

export default Home;

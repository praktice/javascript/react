import React, { useEffect } from "react";
import ReactDOM from "react-dom";

function UseEffect() {
  useEffect(() => {
    console.log('i have arrived at the party!');
    document.title = 'preesent';
  });

  return <div>stuff goes here</div>;
}

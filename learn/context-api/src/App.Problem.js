import React, { Component } from "react";

/**
 * @desc
 * This shows what's called "Prop Drilling", when your components
 * get too deep and you have to pass props from one component to another
 * every level, it can get too deep.
 *
 * In turn, many resort to a state management tool to grab a global state to use as a prop,
 * such as Redux, MobX, etc. We can use the built-in Context-API.
 */

/**
 *  This is a simple example, with deeper components it can get wild.
 *  <App>
 *    L <Human> (name) passed to
 *        L <Man> (name)
 */
const Human = props => {
  const { name } = props;
  return <Man name={name} />;
};

const Man = props => {
  const { name } = props;
  return <div className="box">{name}</div>;
};

class App extends Component {
  render() {
    return (
      <section className="container">
        <div className="columns">
          <div className="column is-12">
            {/* Human has an inbetween of "Man" component */}
            <Human name="Jefferson" />
          </div>
        </div>
      </section>
    );
  }
}

export default App;

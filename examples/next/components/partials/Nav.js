import Link from 'next/link'
import ActiveLink from '../ActiveLink'

export default function Nav() {
  return (
    <nav className="navbar is-white">
      <div className="container">
        <div className="navbar-brand">
          <Link activeClassName="active"  className="navbar-item brand-text" href="/">
            <a>App</a>
          </Link>
          <div className="navbar-burger burger" data-target="navMenu">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div id="navMenu" className="navbar-menu">
          <div className="navbar-start">
            <ActiveLink activeClassName="active" href="/">
              <a className="navbar-item">Home</a>
            </ActiveLink>
            <ActiveLink activeClassName="active" href="/about">
              <a className="navbar-item">About</a>
            </ActiveLink>
          </div>
        </div>
      </div>
    </nav>
  )
}


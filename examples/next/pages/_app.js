// import '../styles/_variables.scss';
import 'bulma/bulma.sass';
import '../styles/globals.scss'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp

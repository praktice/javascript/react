import React, { useState } from 'react';

function MultiHooks() {
  const [count, setCount] = useState(0);
  const [products, setProducts] = useState([{ name: 'Surfboard', price: 100 }]);
  const [coupon, setCoupon] = useState(null);

  return <div>{/_ use all those things here _/}</div>;
}

export default MultiHooks;

import React from 'react';
import propTypes from 'prop-types';

const DashboardLayout = (props) => {
  const {children} = props;
  return (
    <>
      {children}
    </>
  );
};

DashboardLayout.propTypes = {
  children: propTypes.element.isRequired
}

export default DashboardLayout;

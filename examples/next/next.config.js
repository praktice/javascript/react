const path = require('path')

module.exports = {
  target: 'serverless',
  webpack: function (config) {
    config.module.rules.push({
      test: /\.md$/,
      use: 'raw-loader',
    })
    return config
  },
  sassOptions: {
    includePaths: [
      // @TODO Whihc comes first?
      path.join(__dirname, 'node_modules/react-bulma-components/src/index.sass'),
      path.join(__dirname, 'styles')
    ],
  },
  compilerOptions: {
    baseUrl: "./",
    paths: {
      "@components/*": ["components/*"],
      "@utils/*": ["utils/*"]
    }
  }
}

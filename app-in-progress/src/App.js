import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';


// Pages: Public
// --------------------------------------------------------------
import {
  About,
  Home,
  NotFound,
  Privacy,
  Terms
} from './pages';


// Pages: Auth
// --------------------------------------------------------------
import {
  Login,
  PasswordReset,
  PasswordResetConfirm,
  Signup
} from './pages/Auth';


// Pages: Dashboard (Protected)
// --------------------------------------------------------------
import {
  Dashboard,
  Profile
} from './pages/Dashboard';


// App: Page Container
// --------------------------------------------------------------
class App extends Component {
  render() {
    return (
      <section>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/terms" component={Terms} />
            <Route path="/privacy" component={Privacy} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/dashboard/profile" component={Profile} />
            <Route path="/auth.login" component={Login} />
            <Route path="/auth/signup" component={Signup} />
            <Route path="/auth/password/reset" component={PasswordReset} />
            <Route path="/auth/password/reset/confirm" component={PasswordResetConfirm} />
            <Route component={NotFound} />
          </Switch>
        </BrowserRouter>
      </section>
    );
  }
}

export default App;

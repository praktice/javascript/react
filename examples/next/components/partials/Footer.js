import Link from 'next/link'

export default function Footer() {
  return (
      <footer className="footer">
        <div className="content has-text-centered">
        <p>
          <b>App</b> developed by <Link href="https://jream.com"><a>JREAM</a></Link>
          &copy; 2020
        </p>
        </div>
      </footer>
  )
}

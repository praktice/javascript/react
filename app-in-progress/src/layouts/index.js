import AuthLayout from './AuthLayout';
import DashboardLayout from './DashboardLayout';
import PublicLayout from './PublicLayout';

export default {
  AuthLayout,
  DashboardLayout,
  PublicLayout,
}

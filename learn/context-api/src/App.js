import React, { Component } from "react";

// We need:
//  - (Context)
//        - Must create context
//        - React.createContext())
//  - (Provider)
//        - (Lives at Top,
//        - Must Have: value={{ obj }} or {oneval}
//        - Value is Passed to Consumer below
//   - (Consumer)
//        - Uses data given by provider
//        - Used inside a Component (You dont have to wrap the entire component)
const SuperContext = React.createContext();
class SuperProvider extends Component {
  state = {
    name: "Leeroy",
    age: 22
  };
  render() {
    return (
      <SuperContext.Provider
        value={{
          state: this.state,
          incAge: () => this.setState({ age: this.state.age + 1 })
        }}
      >
        {this.props.children}
      </SuperContext.Provider>
    );
  }
}
/**
 * @desc
 * This shows what's called "Prop Drilling", when your components
 * get too deep and you have to pass props from one component to another
 * every level, it can get too deep.
 *
 * In turn, many resort to a state management tool to grab a global state to use as a prop,
 * such as Redux, MobX, etc. We can use the built-in Context-API.
 */

/**
 *  This is a simple example, with deeper components it can get wild.
 *  <App>
 *    L <Human> (name) passed to
 *        L <Man> (name)
 */
const Human = () => {
  return <Man />;
};

class Man extends Component {
  render() {
    return (
      <div className="box">
        <SuperContext.Consumer>
          {/* Always a function */}
          {context => (
            <>
              <p>
                From Consumer name: <b>{context.state.name}</b>
              </p>
              <p>
                From Consumer age: <b>{context.state.age}</b>
              </p>
              <p>
                <button onClick={context.incAge}>Add to Age</button>
              </p>
            </>
          )}
        </SuperContext.Consumer>
      </div>
    );
  }
}

// With SuperProvider Wrapped, we can access from any child
class App extends Component {
  render() {
    return (
      <SuperProvider>
        <section className="container">
          <div className="columns">
            <div className="column is-12">
              <Human />
            </div>
          </div>
        </section>
      </SuperProvider>
    );
  }
}

export default App;

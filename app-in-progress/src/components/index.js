import Header from './Header';
import Footer from './Footer';
import Navigation from './Navigation';

export default {
  Header,
  Footer,
  Navigation,
}

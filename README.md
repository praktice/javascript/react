# JS-React

React is a Front End Javascript library, like Vue and Angular.
You can see more at reactjs.org

Gatsby is a pre-build Framework around React to make building websites
easier with a handful of plugins. It's quite great.

I use `yarn` (yarnjs) rather than `npm` as it is much faster.

---

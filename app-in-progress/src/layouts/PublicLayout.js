import React from 'react';
import propTypes from 'prop-types';

const PublicLayout = (props) => {
  const {children} = props;
  return (
    <>
      {children}
    </>
  );
};

PublicLayout.propTypes = {
  children: propTypes.element.isRequired
}

export default PublicLayout;

export default function Hero() {
  return (
    <div className="hero is-info is-small">
      <div className="hero-body">
        <div className="container">
          <h1 className="title">
            Hero
          </h1>
          <h2 className="subtitle">
            I hope you are having a great day!
          </h2>
        </div>
      </div>
    </div>
  )
}

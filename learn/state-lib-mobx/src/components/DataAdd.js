import React, { Component } from "react";

class DataAdd extends Component {
  constructor(props) {
    super(props);
    this.props.store.data = {
      name: "",
      salary: ""
    };
  }

  addEmployee = () => {
    const name = prompt("Name:");
    const salary = parseInt(prompt("Salary:"), 10);
    this.props.store.pushEmployee({ name, salary });
  };

  handleChange = (event) => {
    event.preventDefault();
    if (event.target.value === "") {
      return false;
    }
    this.props.store.data[event.target.name] = event.target.value;
  };

  handleSubmit = (event) => {
    event.preventDefault();
    console.log(this.props.store);
    this.props.store.pushEmployee(
      this.props.store.data.name,
      this.props.store.data.salary
    );
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="controls">
          <div className="field">
            <p className="control">
              <input
                type="text"
                className="input is-medium"
                name="name"
                placeholder="Name"
                onChange={this.handleChange}
              />
            </p>
          </div>
          <div className="field">
            <p className="control">
              <input
                type="number"
                className="input is-medium"
                name="salary"
                placeholder="Daily Salary"
                onChange={this.handleChange}
              />
            </p>
          </div>
          <div className="field">
            <p className="control">
              <button className="button is-medium is-info">Add</button>
            </p>
            <p className="error">{this.props.store.errorMsg}</p>
          </div>
        </div>
      </form>
    );
  }
}

export default DataAdd;

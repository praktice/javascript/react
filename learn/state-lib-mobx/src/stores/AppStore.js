import { decorate, observable, action, computed } from "mobx";

class Store {
  employeesList = [
    { name: "John Doe", salary: 150 },
    { name: "Richard Roe", salary: 225 }
  ];

  clearList() {
    this.employeesList = [];
  }

  pushEmployee(e) {
    this.employeesList.push(e);
  }

  get totalSum() {
    let sum = 0;
    this.employeesList.map((e) => (sum = sum + e.salary));
    return sum;
  }
}

decorate(Store, {
  employeesList: observable,
  clearList: action,
  pushEmployee: action,
  totalSum: computed
});

const appStore = new Store();
export default appStore;
export { Store };

import Login from './Login';
import PasswordReset from './PasswordReset';
import PasswordResetConfirm from './PasswordResetConfirm';
import Signup from './Signup';

export default {
  Login,
  PasswordReset,
  PasswordResetConfirm,
  Signup,
}

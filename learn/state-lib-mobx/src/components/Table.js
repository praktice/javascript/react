import React, { Component } from "react";
import { observer } from "mobx-react";

class Table extends Component {
  render() {
    const { store } = this.props;
    return (
      <section>
        <hr />
        <table className="table is-fullwidth is-bordered is-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Daily Salary</th>
            </tr>
          </thead>
          <tbody>
            {store.employeesList.map((e, i) => (
              <tr key={i} data={e}>
                <td>{e.name}</td>
                <td>{e.salary}</td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td>Total:</td>
              <td>{store.totalSum}</td>
            </tr>
          </tfoot>
        </table>
      </section>
    );
  }
}
Table = observer(Table);

export default Table;

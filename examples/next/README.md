# App

## Install

```sh
npm i -g vercel create-next-app
```

## Run

```sh
yarn dev
yarn build
```

## Deploy

```sh
ddd
```

## Special Paths

All deployment URLs have two special pathnames:

```sh
/_src
/_logs
```

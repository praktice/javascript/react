# State

- You are free to add anything to state; React sets up props.
- State is isolated to a class Component.
- State can be updated via function `setState`
  - `setState` merges your updated with the current, don't worry about losing other data.
- State is may be updated asynchronously, and may cause issues (See how to fix)

#### Fixing State Asynchronously

Call it as a function, the previous state and the updated:

```js
// May or may not work async
this.setState({ age: this.state.age + 1 });

// Best: Will work async via function
this.setState((state, props) => ({
  age: state.age + 1
}));
```

#### Class constructors

- Always call the base class with`super(props)`

```js
class X extends Component {
  constructor(props) {
    super(props);
  }
}
```

#### Setting Initial State

- State can have functions
- State can have nested values

```js
class X extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: "value",
      key2: () => {
        return "method";
      },
      nested: {
        property: "is-ok"
      }
    };
  }
}
```

import React, { Component } from "react";
import Table from "./components/Table";
import DataAdd from "./components/DataAdd";
import DataClear from "./components/DataClear";
import appStore from "./stores/AppStore.js";

class App extends Component {
  render() {
    return (
      <section className="section">
        <div className="content">
          <div className="tile is-ancestor">
            <div className="tile is-vertical is-12">
              <div className="tile">
                <div className="tile is-parent is-vertical">
                  <article className="tile is-child notification is-primary">
                    <p className="title">Add to List</p>
                    <DataAdd store={appStore} />
                    <p>TODO: Fix the input</p>
                  </article>
                  <article className="tile is-child notification is-warning">
                    <p className="title">Clear Data</p>
                    <DataClear store={appStore} />
                  </article>
                </div>
                <div className="tile is-parent">
                  <article className="tile is-child notification is-info">
                    <p className="title">Employee Table</p>
                    <Table store={appStore} />
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default App;

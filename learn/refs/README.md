# Refs (References)

- Can only be used on class components.

**There are a few good use cases for refs:**

- Managing focus, text selection, or media playback.
- Triggering imperative animations.
- Integrating with third-party DOM libraries.

**Avoid using refs for anything that can be done declaratively.**

ie: Don't use open() and close() methods on a Dialog component, pass an isOpen prop to it.

## Create Ref

```js
this.myRef = React.createRef();
<div ref={this.myRef} />;
```

## Access Ref

```js
const node = this.myRef.current;
```

import React, { Component } from "react";
import validator from "./validator";

export default class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: {
        value: "",
        placeholder: "",
        touched: false,
        valid: false,
        validationRules: {
          minLength: 20,
          isRequired: true
        }
      },
      email: {
        value: "",
        placeholder: "",
        touched: false,
        valid: false,
        validationRules: {
          minLength: 20,
          isRequired: true
        }
      },
      message: {
        value: "",
        placeholder: "",
        touched: false,
        valid: false,
        validationRules: {
          minLength: 20,
          isRequired: true
        }
      },
      validForm: false,
      submittingForm: false
    };
  }

  handleChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.type === "checkbox" ? target.checked : target.value;

    this.setState({
      [name]: {
        value,
        valid: validator(value, this.state[name].validationRules),
        touched: true
      }
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { name, email, message } = this.state;
    this.setState({ submitting: true });

    // ...

    this.setState({ submitting: false });
  };

  render() {
    const { submittingForm, validForm, name, email, message } = this.state;

    return (
      <form onSubmit={this.handleSubmit}>
        <label>Name</label>
        <input
          type="text"
          name="name"
          touched={this.state.name.touched}
          valid={this.state.name.valid}
          value={name}
          onChange={this.handleChange}
        />

        <label>Email</label>
        <input
          type="email"
          name="email"
          touched={this.state.email.touched}
          valid={this.state.email.valid}
          value={email}
          onChange={this.handleChange}
        />

        <label>Message</label>
        <textarea
          name="message"
          touched={this.state.message.touched}
          valid={this.state.message.valid}
          onChange={this.handleChange}
        >
          {message}
        </textarea>

        <button {!validForm || !submittingForm && dis } type="submit">Send</button>
      </form>
    );
  }
}

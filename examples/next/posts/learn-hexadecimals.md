---
title: 'Learn Hexadecimals'
author: 'Jesse'
---

```
0123456789
A(10)B(11)C(12)D(13)E(14)F(15)
```

I was just thinking about Hexadecimals and wondered how it calculate a color
value. Standard RGB has a range of 0 to 255 for each color, the higher the number means
the more colors is applied. It's a color mixer based on a numeric value, well DECHEX value.
It's not that complicated but I am missing the last part.

RGB has a numberic value for Red, Green Blue. It goes from 0 to 255 for each color.
In order to reach that it needs two place holders for each colors so RED GREEN BLUE is
three (3) colors both using two (2) placeholders, so the total is six (6) values appearing
as: 000000, A02A43, FFB323, etc.

## Total
F being 15 times the value beside it; so FF is (15x15) or 225.




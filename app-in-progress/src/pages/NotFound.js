import React from 'react';
import {PublicLayout as Layout} from '../layouts';
import Header from '../components/Header';

const NotFound = () => {
  return (
    <Layout>
    <Header title="NotFound" />
    <p>
      Hello
    </p>
    </Layout>
  );
};

export default NotFound;

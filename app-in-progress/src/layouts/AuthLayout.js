import React from 'react';
import propTypes from 'prop-types';

const AuthLayout = (props) => {
  const {children} = props;
  return (
    <>
      <h1>AuthLayout</h1>
      {children}
    </>
  );
};

AuthLayout.propTypes = {
  children: propTypes.element.isRequired
}

export default AuthLayout;

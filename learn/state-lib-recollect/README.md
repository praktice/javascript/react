
# Store Object

- [React-Recollect](https://github.com/davidgilbertson/react-recollect#the-collect-function)
- import, read, and write to the store in any file
  - `import { store } from 'react-recollect';`
- Options:
  - You can use as a plain JS Object
  - The Store is immutable, it will create a **new object** just like redux and others.
  - This can be separated into a store area, I created a `store-optional` folder.
- In React:
  - You can wrap `export default collect(ComponentName)`
  - Refer to the  `store` within a component via `prop`, **not** the store imported from `react-recollect`

## Examples as JS Object

```js
store.tasks = ['one', 'two', 'three']; // Fine
store.tasks.push('four'); // ...
if ('tasks' in store) // ...
delete store.tasks; // ...
```

## Example in React Component

- Passing a Reference from a child to a parent:

### Child

```js
const MyInput = props => (
  <label>
    Some input
    <input ref={props.forwardedRef} /> {* <-- HERE *}
  </label>
);

// BELOW --->
export default collect(MyInput, { forwardRef: true });
```

### Parent

```js
class MyParentComponent extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef(); // <-- From
  }

  render () {
    return (
      <div>
        <MyInput ref={this.inputRef}/>

        <button
          onClick={() => {
            this.inputRef.current.focus()
          }}
        >
          Focus the input for some reason
        </button>
      </div>
    );
  }
}
```

# FT Hooks

Reacts Hooks are a new feature, not yet released at time of writing.

- **Hooks Allow**
  - React Features in Functions (Not only Classes)
  - This Includes: `State` and `Lifecycle`


## What is: useState

`useState` provides two variables named anything we want.

- First Var = Value (Kinda like this.state)
- Second Var = Function (Update the First Var Value, Kinda like this.setState)
- Lastly, useState is the argument that we pass to it. The useState argument is the initial state value.
  - EG:

```js
//   Value     Function         Initial Value for count
//      |        |                 |
//      V        V                 V
const [count, setCount] = useState(0);
```

## What is Effect Hook?

- Effects will run after every render, including the initial.
- **Effects are similar to**
  - `componentDidMount`
  - `componentDidUpdate`
  - `componentWillUnmount`
- **Used For:**
  - Fetching data
  - Manually changing the DOM (document title)
  - Setting up a subscription

**componentDidMount: Runs once**

```js
// only run on mount. pass an empty array
useEffect(() => {
  // only runs once
}, []);
```

**componentDidUpdate: Runs on changes**

```js
// only run if count changes
useEffect(
  () => {
    // run here if count changes
  },
  [count]
);
```

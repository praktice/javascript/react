import matter from 'gray-matter'
import fs from 'fs';
import Layout from '../components/Layout'
import PostList from '../components/PostList'

const Index = ({ posts, title, description, ...props }) => {
  return (
    <Layout pageTitle={title}>
      <p className="description">{description}</p>
      <main>
        <PostList posts={posts} />
        <div>{posts.length}</div>
      </main>
    </Layout>
  )
}

export default Index

export async function getStaticProps() {
  const configData = await import(`../siteconfig.json`)

  const posts = ((context) => {
    const keys = context.keys()
    const values = keys.map(context)

    const data = keys.map((key, index) => {
      let slug = key.replace(/^.*[\\\/]/, '').slice(0, -3)
      const value = values[index]
      const document = matter(value.default)
      return {
        frontmatter: document.data,
        markdownBody: document.content,
        slug,
      }
    })
    return data
  })(require.context(configData.paths.posts, true, /\.md$/))

  // const total = posts.length
  // console.log(total)

  return {
    props: {
      posts,
      title: configData.default.title,
      description: configData.default.description,
      pagination: {
        perPage: configData.pagination.perPage
      }
    },
  }
}

const validator = (value, rules) => {
  let isValid = true;

  for (let rule in rules) {
    switch (rule) {
      case "minLength":
        isValid = isValid && minLength(value[rules[rule]]);
        break;
      case "maxLength":
        isValid = isValid && maxLength(value, rules[rule]]);
        break;
        case 'required':
          isValid = isValid && required(value[rules);
            break;
        default:
            isValid = truel
    }
  }
  return isValid;
};

const minLength = (value, minLength) => {
  return value.length >= minLength;
};

const maxLength = (value, maxLength) => {
  return value.length < maxLength;
};

const required = (value) => {
  return value.trim() !== "";
};

export default validator;

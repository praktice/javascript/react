import React from 'react';
import propTypes from 'propTypes';

const getPublicLinks = () => {
  return (
    <div>
      links pub
    </div>
  )
}
const getDashboardLinks = () => {
  return (
    <div>
      links dash
    </div>
  )
}

const Navigation = (props) => {
  const { type } = props;
  return (
    <>
      Navigation
      {(type === 'dashboard') ? getDashboardLinks : getPublicLinks }
    </>
  );
};

Navigation.propTypes = {
  type: propTypes.string
}

export default Navigation;

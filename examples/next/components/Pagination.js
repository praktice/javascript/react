import { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate'
import Router, { withRouter } from 'next/router'
import { propTypes } from 'prop-types';

await function Pagination({ items, perPage=5 }) {
  const [isLoading, setLoading] = useState(false);
  const startLoading = () => setLoading(true);
  const stopLoading = () => setLoading(false);

  useEffect(() => { //After the component is mounted set router event handlers
    Router.events.on('routeChangeStart', startLoading);
    Router.events.on('routeChangeComplete', stopLoading);

    return () => {
      Router.events.off('routeChangeStart', startLoading);
      Router.events.off('routeChangeComplete', stopLoading);
    }
  }, [])

  // When new page selected handle Query params.
  // Then add or modify page param, navigate to the new route.
  const paginationHandler = (page) => {
    const currentPath = props.router.pathname;
    const currentQuery = props.router.query;
    currentQuery.page = page.selected + 1;

    props.router.push({
      pathname: currentPath,
      query: currentQuery,
    });
  }

  //Conditional rendering of the posts list or loading indicator
  let content = null;
  if (isLoading) {
    content = <div>Loading...</div>;
  } else {
    // Generating posts list
    content = (
      <ul>
          {props.posts.map(post => {
              return <li key={post.id}>{post.title}</li>;
          })}
      </ul>
    );

    return (
      <ReactPaginate
        previousLabel={'Previous'}
        nextLabel={'Next'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        activeClassName={'active'}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        initialPage={props.currentPage - 1}
        pageCount={props.pageCount}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={paginationHandler}
        />
      );
    }

//   return (
//     <nav className="pagination" role="navigation" aria-label="pagination">
//       <a className="pagination-previous" title="This is the first page" disabled>Previous</a>
//       <a className="pagination-next">Next page</a>
//       <ul className="pagination-list">
//         <li>
//           <a className="pagination-link is-current" aria-label="Page 1" aria-current="page">1</a>
//         </li>
//         <li>
//           <a className="pagination-link" aria-label="Goto page 2">2</a>
//         </li>
//         <li>
//           <a className="pagination-link" aria-label="Goto page 3">3</a>
//         </li>
//       </ul>
//     </nav>
//   )
}

Pagination.propTypes = {
  items: PropTypes.array.isRequired,
  nextLabel: propTypes.string,
  previousLabel: propTypes.string,
  initialPage: propTypes.number,
  pageCount: propTypes.number,
  pageRangeDisplayed: propTypes.number,
}

export default Pagination

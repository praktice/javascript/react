import React from 'react';
import propTypes from 'propTypes';

const Header = (props) => {
  const { title } = props;
  return (
    <>
      hi {title}
    </>
  );
};

Header.propTypes = {
  title: propTypes.string.isRequired
}

export default Header;

import React, { Component } from 'react';
import propTypes from 'prop-types';

class Task extends Component {

  handleClick(event) => {
    event.preventDefault();
    this.state.incAge()
  }

  render() {
    <SuperContext.Consumer>
      <button onClick="handleClick">Click Me</button>
    </SuperContext.Consumer>
  }

}
